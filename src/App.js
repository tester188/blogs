import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Signin from './pages/signin';
import Signup from './pages/signup';

function App() {
  return (
    <BrowserRouter>
   
    <Routes>
       <Route path='/' element={<Signin />} />
       <Route path='/signup' element={<Signup />} />

    </Routes>

    {/* this container is used to show toast messages */}
    {/* <ToastContainer position='top-center' autoClose={1000} /> */}
  </BrowserRouter>
  );
}

export default App;
